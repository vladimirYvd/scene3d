//
//  Model3dView.swift
//  Scanner
//
//  Created by Vladimir Evdokimov on 2019-03-13.
//  Copyright © 2019 Orthogenic Laboratories. All rights reserved.
//

import Foundation
import SceneKit

open class Scene3dView: UIView {
    public var sceneView: SCNView?
    public var scene: SCNScene = SCNScene()
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        initScene()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initScene()
    }
    
    //MARK: - Public

    open func present(node:SCNNode?, in rootNode:SCNNode? = nil, scale:Int = 1, only: Bool = false) {
        if only {
            clearExceptCameras()
        }

        let rootNode = (rootNode != nil) ? rootNode : scene.rootNode

        if let node = node {
            node.scale = SCNVector3(scale, scale, scale)
            rootNode?.addChildNode(node)
        }
    }
    
    public func clearExceptCameras() {
        scene.rootNode.childNodes.forEach({ if $0.camera == nil { $0.removeFromParentNode() } })
    }

    public func clearAll() {
        scene.rootNode.childNodes.forEach({ $0.removeFromParentNode() })

    }

    open func initScene() {
        self.clipsToBounds = true
        sceneView = SCNView(frame: self.frame)
        sceneView?.allowsCameraControl = true
        sceneView?.autoenablesDefaultLighting = true
        sceneView?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        if let view = sceneView {
            view.backgroundColor = .clear
            self.addSubview(view)
        }
        sceneView?.showsStatistics = true
        
        sceneView?.gestureRecognizers?.forEach({
            if $0.isKind(of: UIPanGestureRecognizer.self) {
                sceneView?.gestureRecognizers = [$0]
                return
            }
        })
        
        sceneView?.prepare([scene as Any], completionHandler: {[weak self] (success) in
            guard success else {return}
            guard let weak = self else {return}
            
            weak.sceneView?.scene = weak.scene
        
            weak.scene.rootNode.childNodes.forEach({$0.removeFromParentNode()})
        })
    }
}
